import numpy as np
import sys


def read_from_input_file(filename):
    #   Read from the input file
    with open(filename, 'r') as f:
        read_line = f.readline()
        r, c, l, h = [int(n) for n in read_line.split()]

        pizza = np.zeros([r, c])
        for i in range(r):
            for ing, j in zip(f.readline(), range(c)):
                if ing == 'T':
                    pizza[i, j] = 1
                else:
                    pizza[i, j] = 0
    
    return pizza, r, c, l, h


class CutThePizza:
    
    def __init__(self, pizza, r, c, l, h):
        # parameters
        self.pizza = pizza
        self.r = r
        self.c = c
        self.L = l
        self.H = h
        self.pieces = []

    def piece_is_within_pizza_area(self, r, c):
        try:
            assert (r[0] >= 0) and (c[0] >= 0)
            self.pizza[r[0], c[0]]
            self.pizza[r[1], c[1]]
            return True
        except:
            return False
    
    def piece_has_maximum_of_h_cells(self, r, c):
        # Check if the piece has a maximum of H cells and has not included cells of other pieces.
        legal = False
        if self.piece_is_within_pizza_area(r, c):
            piece = self.pizza[r[0]:r[1]+1, c[0]:c[1]+1]
            if piece.size <= self.H and not np.isnan(piece.sum()):
                # if there's one or more cells with NaN value, the sum is NaN
                legal = True    
        return legal
    
    def piece_has_minimum_of_l_cells_of_each_ingredient(self, r, c):
        # Check if the piece has a minimum of L cells of each ingredient
        piece = self.pizza[r[0]:(r[1]+1), c[0]:(c[1]+1)]    # piece of pizza
        tomatoes = np.sum(piece)
        mushrooms = np.size(piece) - tomatoes
        
        if (tomatoes >= self.L) and (mushrooms >= self.L):
            return True
        else:
            return False
    
    @staticmethod
    def expand_piece(r, c, nav):
        # Expand the pizza piece from a given point to the direction given
        new_r = list(r)
        new_c = list(c)
        next_dir = ''
        if nav == 'right':
            new_c[1] += 1
            next_dir = 'down'
        elif nav == 'down':
            new_r[1] += 1
            next_dir = 'left'
        elif nav == 'left':
            new_c[0] -= 1
            next_dir = 'up'
        elif nav == 'up':
            new_r[0] -= 1
            next_dir = 'right'
        return new_r, new_c, next_dir

    def new_piece(self, vertex):
        # Generate a new piece to be cut
        r_final = [vertex[0], vertex[0]]
        c_final = [vertex[1], vertex[1]]
        nav = 'right'
        out = False
        
        while not out:
            legal = False
            counter = 0
            while counter < 3:
                # Try to enlarge the piece in all four directions
                r, c, nav = self.expand_piece(r_final, c_final, nav)
                if self.piece_has_maximum_of_h_cells(r, c):
                    legal = True
                    r_final = r
                    c_final = c
                    print(r_final, c_final)
                    break
                counter += 1
            if not legal:
                out = True
        
        right_amount = self.piece_has_minimum_of_l_cells_of_each_ingredient(r_final, c_final)
        return right_amount, r_final, c_final
    
    def cut_pizza_pieces(self):
        # Try to cut correct pieces out of the pizza maximizing the total number of cells in all pieces
        for i in range(self.r):
            for j in range(self.c):
                if not np.isnan(self.pizza[i][j]):
                    right_amount, r, c = self.new_piece([i, j])
                    if right_amount:
                        print("Right amount of ingredients achieved")
                        self.pieces += [[r[0], c[0], r[1], c[1]]]
                        # Fill with NaN where a piece has already been generated on the pizza
                        piece = np.empty([r[1] - r[0] + 1, c[1] - c[0] + 1])
                        piece[:] = None
                        self.pizza[r[0]:r[1] + 1, c[0]:c[1] + 1] = piece
        print(self.pizza)


def write_to_output_file(condition, filename):
    # Write the output to a file
    with open(filename, 'w') as f:
        f.write('{}\n'.format(len(condition.pieces)))
        for piece in condition.pieces:
            f.write(' '.join([str(item) for item in piece]) + '\n')


def main():
    print('Tech solution to cutting pizza with the right amount of tomatoes and mushrooms on each piece'
          '\nReading from input file %s' % sys.argv[1])

    pizza, r, c, l, h = read_from_input_file(sys.argv[1])
    condition = CutThePizza(pizza, r, c, l, h)
    condition.cut_pizza_pieces()

    print('\nWriting on output file %s' % sys.argv[2], '...')
    write_to_output_file(condition, sys.argv[2])


if __name__ == '__main__':
    main()
