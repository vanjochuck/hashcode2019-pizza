# Hashcode2019-pizza

This is a solution for the Practice Round in Google Hash Code 2019 developed by the team thefive.

The problem statement is in the file "pizza.pdf".

To run the program with the input file *input/b_small.in* and write the output in *output/b_small.out*:

1. Navigate to your project root folder in terminal

2. Run `$ python pizza.py input/b_small.in output/b_small.out`